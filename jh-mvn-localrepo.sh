#!/bin/bash
# Copyright © 2013 Luke Shumaker <lukeshu@sbcglobal.net>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the COPYING file for more details.

if [[ $# -gt 0 ]]; then
	jh-help $0 >>/dev/stderr
	exit 1
fi

if [ -e /etc/maven/settings.xml ]; then
	xml sel -T -t -v //_:localRepository -n /etc/maven/settings.xml
else
	echo /usr/share/maven/repository
fi
