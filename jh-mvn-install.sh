#!/bin/bash
# Copyright © 2013 Luke Shumaker <lukeshu@sbcglobal.net>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the COPYING file for more details.

if [[ $# -lt 4 ]]; then
	jh-help $0 >>/dev/stderr
	exit 1
fi

base=`jh-mvn-basename $1 $2 $3`
pomfile=$4
jarfile=$5
if shift 5; then
	aliases=("$@")
fi

dir="${base%/*}"
install -d "${DESTDIR}${dir}"
install -m 644 "$pomfile" "${DESTDIR}${base}.pom"
[[ -n $jarfile ]] || exit 0
install -m 644 "$jarfile" "${DESTDIR}${base}.jar"

if [[ -n "${aliases[*]}" ]]; then
	for alias in "${aliases[@]}"; do
		link="/usr/share/java/$alias"
		install -d "${DESTDIR}${link%/*}"
		ln -s "${base}.jar" "${DESTDIR}${link}"
	done
fi
