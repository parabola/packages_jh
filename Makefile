prefix = /usr
bindir = $(prefix)/bin
jh-bindir = $(prefix)/lib/jh

subcommands = help list-commands mvn-basename mvn-install mvn-localrepo checksource
dirs = $(DESTDIR)$(bindir) $(DESTDIR)$(jh-bindir)

####

subcommand_files = $(addprefix jh-,$(subcommands))
help_files = $(addsuffix .help.txt,$(subcommand_files))

all: PHONY build

####

build: PHONY jh $(subcommand_files)
install: PHONY $(DESTDIR)$(bindir)/jh $(addprefix $(DESTDIR)$(jh-bindir)/,$(subcommand_files) $(help_files))
clean: PHONY
	rm -f jh $(subcommand_files)

jh.sh: jh.sh.in
	sed 's|@jh-bindir@|$(jh-bindir)|g' < $< > $@

$(DESTDIR)$(jh-bindir)/%: % | $(DESTDIR)$(jh-bindir)
	cp '$<' '$@'

$(DESTDIR)$(bindir)/jh: jh | $(DESTDIR)$(bindir)
	cp '$<' '$@'

$(dirs):
	install -d '$@'

####

.PHONY: PHONY FORCE
PHONY FORCE: ; @:
