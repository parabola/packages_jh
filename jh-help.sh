#!/bin/bash
# Copyright © 2013 Luke Shumaker <lukeshu@sbcglobal.net>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the COPYING file for more details.

stem=jh

master_help() {
	echo "Usage: $jh_short <command> [command-specific-arguments]"
	echo "Java helper for PKGBUILDs"
	echo ""
	jh-list-commands|sed 's/./  &/'
}

generic_help() {
	local cmd=$1
	cmd=${cmd##*/}
	cmd=${cmd#$stem-}

	file=`find ${PATH//:/ } -type f -name "$stem-$cmd.help.txt" 2>/dev/null`
	if [[ -r $file ]]; then
		sed "s|@cmd@|$jh_short|g" "$file"
	else
		echo "$jh_short: Cannot find help file for '$cmd'" >> /dev/stderr
		exit 1
	fi
}

case $# in
	0) master_help; exit 0;;
	1) generic_help "$1"; exit 0;;
	*) generic_help help; exit 1;;
esac
