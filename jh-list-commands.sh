#!/bin/bash
# Copyright © 2013 Luke Shumaker <lukeshu@sbcglobal.net>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the COPYING file for more details.

if [[ $# -gt 0 ]]; then
	jh-help $0 >>/dev/stderr
	exit 1
fi

stem=jh

find ${PATH//:/ } -type f -name "$stem-*.help.txt" 2>/dev/null |
while read cmd_help; do
	cmd=$cmd_help
	cmd=${cmd%.help.txt}
	cmd=${cmd##*/$stem-}
	help=`sed -n 2p "$cmd_help"`
	printf '%-20s  %s\n' "$cmd" "$help"
done | sort
