#!/bin/bash
# Copyright © 2013 Luke Shumaker <lukeshu@sbcglobal.net>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the COPYING file for more details.

if [[ $# != 3 ]]; then
	jh-help $0 >>/dev/stderr
	exit 1
fi

mvn_repo=`jh-mvn-localrepo`

mvn_groupId=$1
mvn_artifactId=$2
mvn_version=$3

dir="${mvn_repo}/${mvn_groupId//.//}/${mvn_artifactId}/${mvn_version}"
file="${mvn_artifactId}-${mvn_version}"

echo "${dir}/${file}"
